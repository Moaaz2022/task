package vodafoneWebsite.browser;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import vodafoneWebsite.driver.WebDriverHandler;
import vodafoneWebsite.vodafoneWebsite;

import java.util.Set;

public class Browser {

    private final WebDriver webDriver = WebDriverHandler.getWebDriver();

    public vodafoneWebsite vodafoneWebsite;

    public Browser() {
        vodafoneWebsite = new vodafoneWebsite();
    }

    public static void scrollToElement(WebDriver driver, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

}

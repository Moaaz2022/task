package vodafoneWebsite;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import vodafoneWebsite.pages.BasePage;
import vodafoneWebsite.pages.*;

public class vodafoneWebsite extends BasePage {

    @FindBy(xpath = "//*[@id=\"userProfileMenu\"]/button")
    private WebElement loginIcon;

    public Home home;
    public Login login;
    public Product product;
    public Cart cart;
    public Checkout checkout;

    public Login clickOnLoginIcon() {
        loginIcon.click();
        return new Login();
    }

}

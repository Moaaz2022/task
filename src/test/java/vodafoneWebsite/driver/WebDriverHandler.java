package vodafoneWebsite.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import vodafoneWebsite.utilities.ConfigProperties;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class WebDriverHandler {

    private static WebDriver webDriver;

    private static String configrationFile = "resources/configFiles/configrationFile.properties";
    private String driverType = "driverType";
    private String url="WebsiteLink";


    public WebDriverHandler() throws IOException {
        getDriverType();
    }

    private void getDriverType() throws IOException {
        ConfigProperties configBrowser = new ConfigProperties(configrationFile);
        driverType = configBrowser.getProperty(driverType);

      if(driverType.equals("chrome")) {
          WebDriverManager.chromedriver().browserVersion("119.0.6045.200").setup();
          webDriver = new ChromeDriver();
          webDriver.manage().window().maximize();
      }
        else {
            WebDriverManager.firefoxdriver().setup();
            webDriver=new FirefoxDriver();
            webDriver.manage().window().maximize();
        }

    }
    public static WebDriver getWebDriver() {

        return webDriver;
    }

    public void navigateTo(String link) {
        webDriver.navigate().to(link);
    }

    public void resetCache() {
        webDriver.manage().deleteAllCookies();
    }

    public static void close() {
        webDriver.quit();
    }

    public static void switchToNewTab(){
        String originalTabHandle= webDriver.getWindowHandle();
        Set<String> allTabHandles=webDriver.getWindowHandles();
        for(String tabHandle : allTabHandles){
            if(!tabHandle.equals(originalTabHandle)){
                webDriver.switchTo().window(tabHandle);
                break;
            }
        }
    }


}

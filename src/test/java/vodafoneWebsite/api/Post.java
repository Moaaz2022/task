package vodafoneWebsite.api;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Post {

    @Test
    public void registerUser(){
        RestAssured.baseURI = "https://reqres.in/api";

        String requestBody = "{\"email\": \"eve.holt@reqres.in\", \"password\": \"pistol\"}";

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post("/register");
        Assert.assertEquals(response.getStatusCode(),200);

    }
}

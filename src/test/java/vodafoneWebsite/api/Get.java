package vodafoneWebsite.api;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;


public class Get {

    @Test
    public void validateResponseCode(){

        given().get("https://reqres.in/api/users?page=2").then().
                assertThat().statusCode(200);
    }
    @Test
    public void validateResponseBody(){
        given().get("https://reqres.in/api/users?page=2").then().
                assertThat().body("data[0].first_name", equalTo("Michael"));
    }
}

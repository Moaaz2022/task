package vodafoneWebsite.api;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class Delete {
    @Test
    public void validateResponseCode() {

        given().delete("https://reqres.in/api/users/2").then().
                assertThat().statusCode(204);
    }

}

package vodafoneWebsite.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login extends BasePage{
    @FindBy(id="username")
    private WebElement msisdn;

    @FindBy(id="password")
    private WebElement password;

    @FindBy(id="submitBtn")
    private WebElement loginButton;

    public WebElement getMsisdn() {
        return msisdn;
    }
    public WebElement getPassword() {
        return password;
    }
    public WebElement getLoginButton() {
        return loginButton;
    }

    public void enterMsisdn(String msisdn) {
        getMsisdn().sendKeys(msisdn);
    }
    public void enterPassword(String password) {
        getPassword().sendKeys(password);
    }
    public Home clickOnLoginButton(){
        loginButton.click();
        return new Home();
    }
}

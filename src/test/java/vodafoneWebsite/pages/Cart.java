package vodafoneWebsite.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Cart extends BasePage{

    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-my-cart/div/div/div/div[2]/vf-cart-order-summary/div/div/div[2]/button[1]")
    private WebElement goToCheckoutButton;

    public Checkout clickOnGoToCheckoutButton(){
        goToCheckoutButton.click();
        return new Checkout();
    }


}

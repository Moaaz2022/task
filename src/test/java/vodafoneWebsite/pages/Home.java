package vodafoneWebsite.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import vodafoneWebsite.driver.WebDriverHandler;

public class Home extends BasePage{

    @FindBy(id = "searchInput")
    private WebElement searchBar;

    @FindBy(xpath = "//*[contains(text(), 'In Smart Phones')]")
    private WebElement samsungSmartPhones;

    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-product-list-page/div[2]/div[2]/div[2]/div[8]")
    private WebElement oppoTab;

    @FindBy(xpath = "//*[contains(text(), 'OPPO Smart Phone A98 (5G)')]")
    private WebElement oppoA98;

    @FindBy(xpath = "/html/body/vf-root/main/section[1]/vf-nav-bar/nav/div/div[3]/vf-cart/div/button")
    private WebElement cartButton;

    public WebElement getSearchBar() {
        return searchBar;
    }

    public void enterSearchBar(String search) {
        getSearchBar().sendKeys(search);
    }

    public void clickOnSamsungSmartPones(){
        samsungSmartPhones.click();
    }

    public void clickOnOppoTab(){
        oppoTab.click();
    }
    public Product clickOnOppoA98(){
        oppoA98.click();
        return new Product();
    }
    public Cart clickOnCartButton(){
        cartButton.click();
        return new Cart();
    }




}

package vodafoneWebsite.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Checkout extends BasePage{

    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div[2]/div")
    private WebElement addNewAddressButton;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/button[1]")
    private WebElement saveAddressButton;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[1]/div[1]/select")
    private WebElement cityDropDownList;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[1]/div[2]/select")
    private WebElement districtDropDownList;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[1]/div[2]/select")
    private WebElement streetName;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[3]/div[1]/input")
    private WebElement buildingNumber;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[3]/div[2]/input")
    private WebElement floorNumber;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[3]/div[3]/input")
    private WebElement apartmentNumber;
    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-home/section/vf-shipping/section/div[2]/div/div/div[1]/div/form/div[3]/div[1]/p")
    private WebElement errorMessage;


    public WebElement getSaveAddressButton(){
       return saveAddressButton;
    }

    public WebElement getCityDropDownList(){
        return cityDropDownList;
    }

    public WebElement getDistrictDropDownList(){
        return districtDropDownList;
    }
    public WebElement getStreetName(){
        return streetName;
    }
    public WebElement getBuildingNumber(){
        return buildingNumber;
    }
    public WebElement getFloorNumber(){
        return floorNumber;
    }
    public WebElement getApartmentNumber(){
        return apartmentNumber;
    }

    public WebElement getErrorMessage(){
        return errorMessage;
    }

    public void clickOnAddNewAddressButton(){
        addNewAddressButton.click();
    }

    public void enterStreetName(String streetName) {
        getStreetName().sendKeys(streetName);
    }
    public void enterBuildingNumber(String buildingNumber) {
        getBuildingNumber().sendKeys(buildingNumber);
    }
    public void enterApartmentNumber(String apartmentNumber) {
        getApartmentNumber().sendKeys(apartmentNumber);
    }
    public void enterFloorNumber(String floorNumber) {
        getFloorNumber().sendKeys(floorNumber);
    }
}

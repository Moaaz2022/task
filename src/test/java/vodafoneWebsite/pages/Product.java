package vodafoneWebsite.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Product extends BasePage{

    @FindBy(xpath="/html/body/vf-root/main/section[2]/vf-middleware/vf-product-details-page/div[2]/div/div/div[1]/div[3]/div[3]/div/div/button[1]")
    private WebElement firstColor;

    @FindBy(xpath="/html/body/vf-root/main/section[2]/vf-middleware/vf-product-details-page/div[2]/div/div/div[1]/div[2]/div/div[1]/div[3]/div/div/button[2]")
    private WebElement secondColor;

    @FindBy(xpath = "//*[contains(text(), 'Out Of Stock ')]")
    private WebElement outOfStock;

    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-middleware/vf-product-details-page/div[2]/div/div/div[1]/div[3]/div[8]/button[1]")
    private WebElement addToCart;

    @FindBy(xpath = "/html/body/vf-root/main/section[2]/vf-middleware/vf-product-details-page/div[2]/div/div/div[1]/div[3]/div[8]/button[2]")
    private WebElement buyNow;

    @FindBy(xpath = "/html/body/vf-root/main/section[1]/vf-alert-box/div/div")
    private WebElement addedToCartAlert;

    @FindBy(xpath = "/html/body/vf-root/main/section[1]/vf-nav-bar/nav/div/div[3]/vf-cart/div/button")
    private WebElement cartButton;

    public WebElement getFirstColor(){
        return firstColor;
    }
    public WebElement getSecondColor(){
        return secondColor;
    }
    public WebElement getOutOfStock(){
        return outOfStock;
    }
    public WebElement getAddToCart(){
        return addToCart;
    }
    public WebElement getBuyNow(){
        return buyNow;
    }
    public WebElement getAddedToCartAlert(){
        return addedToCartAlert;
    }
    public void clickOnFirstColor(){
        firstColor.click();
    }

    public void clickOnsecondColor(){
        secondColor.click();
    }
    public void clickOnAddToCart(){
        addToCart.click();
    }

    public Cart clickOnCartButton(){
        cartButton.click();
        return new Cart();
    }

}

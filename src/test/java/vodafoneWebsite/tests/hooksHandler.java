package vodafoneWebsite.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import vodafoneWebsite.browser.Browser;
import vodafoneWebsite.driver.WebDriverHandler;
import vodafoneWebsite.utilities.ConfigProperties;
import io.cucumber.java.*;
import java.io.IOException;
import java.util.List;

public class hooksHandler extends BaseTest{
    private String url="WebsiteLink";
    @Before(order = 1)
    public void setUpVodafoneWebsite() throws IOException, InterruptedException {
        webDriverHandler = new WebDriverHandler();
        browser = new Browser();
        ratePlans = new ConfigProperties("resources/configFiles/configrateplan.properties");
        configrationFile = new ConfigProperties("resources/configFiles/configrationFile.properties");
        url = configrationFile.getProperty(url);
        webDriverHandler.resetCache();
        webDriverHandler.navigateTo(url);
        removeCookiesPopup = true;
    }
    @Before(order = 2)
    public void acceptAllCookies() {
        WebDriverWait wait = new WebDriverWait(WebDriverHandler.getWebDriver(), 5);
        By elementLocator = By.id("onetrust-banner-sdk");
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
        List<WebElement> cookiesPopup = WebDriverHandler.getWebDriver().findElements(By.id("onetrust-banner-sdk"));
        if (removeCookiesPopup && cookiesPopup.size() > 0) {
            String popupInjectionScript = "document.getElementById('onetrust-accept-btn-handler').click()";
            ((JavascriptExecutor) WebDriverHandler.getWebDriver()).executeScript(popupInjectionScript);
            removeCookiesPopup = false;
        }
    }
    @Before(order = 3)
    public void logIn(){
        WebDriverWait wait = new WebDriverWait(WebDriverHandler.getWebDriver(), 5);
        browser.vodafoneWebsite.login=browser.vodafoneWebsite.clickOnLoginIcon();
        browser.vodafoneWebsite.login.enterMsisdn(ratePlans.getProperty("msisdn"));
        browser.vodafoneWebsite.login.enterPassword(ratePlans.getProperty("password"));
        wait.until(ExpectedConditions.elementToBeClickable(browser.vodafoneWebsite.login.getLoginButton()));
        browser.vodafoneWebsite.home= browser.vodafoneWebsite.login.clickOnLoginButton();
    }

    @After()
    public void tearDown() {
        webDriverHandler.resetCache();
        WebDriverHandler.close();
    }
}

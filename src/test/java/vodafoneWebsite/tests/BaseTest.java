package vodafoneWebsite.tests;

import vodafoneWebsite.browser.Browser;
import vodafoneWebsite.utilities.ConfigProperties;
import vodafoneWebsite.driver.WebDriverHandler;

public class BaseTest {
    protected WebDriverHandler webDriverHandler;
    protected static Browser browser;
    protected static ConfigProperties ratePlans;
    protected static ConfigProperties configrationFile;
    protected static boolean removeCookiesPopup;
}

Feature: TestCases

  Scenario Outline: Test Case 1
    Given website is opened
    When Write in search bar "<search>"
    And select from search list Samsung in Smart Phones
    And Select Oppo Tab
    And Select the following product OPPO Smart Phone A98
    And Select the product color "<color>"
    Then validate the product availability and add it to the cart
    When click on add to cart
    Then the product added to cart successfully
    Examples:
      | search  | color       |
      | samsung | firstColor  |
      | samsung | secondColor |

  Scenario Outline: Test Case 2
    Given website is opened
    When click on cart button
    And Click on Go to checkout button
    And Click on Add new Address
    And Set city District and street name
    And enter invalid Building "<building>" Floor "<floor>" and Apartment values "<apartment>"
    Then Error message displayed and Save address button is dimmed

    Examples:
      | building | floor   | apartment     |
      | 12345    | 12345   | 1234567891011 |
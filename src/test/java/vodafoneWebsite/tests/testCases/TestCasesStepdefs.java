package vodafoneWebsite.tests.testCases;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import vodafoneWebsite.pages.BasePage;
import vodafoneWebsite.tests.BaseTest;


public class TestCasesStepdefs extends BaseTest {
    protected WebDriver driver;
    protected static boolean availability =true;
    @Given("website is opened")
    public void websiteIsOpened() {
        BasePage.waitUntilPageIsLoaded();
    }
    @When("Write in search bar {string}")
    public void writeInSearchBar(String searchQuery) throws InterruptedException {
        browser.vodafoneWebsite.home.enterSearchBar("samsung");
        Thread.sleep(5000);
    }

    @When("select from search list Samsung in Smart Phones")
    public void selectSamsungInSmartPhones() throws InterruptedException {
        browser.vodafoneWebsite.home.clickOnSamsungSmartPones();
        Thread.sleep(5000);
    }

    @When("Select Oppo Tab")
    public void selectOppoTab() throws InterruptedException {
        browser.vodafoneWebsite.home.clickOnOppoTab();
        Thread.sleep(5000);
    }

    @When("Select the following product OPPO Smart Phone A98")
    public void selectOppoSmartPhone() throws InterruptedException {
        browser.vodafoneWebsite.product = browser.vodafoneWebsite.home.clickOnOppoA98();
        Thread.sleep(5000);
    }

    @When("Select the product color {string}")
    public void selectProductColor(String color) {
        System.out.println(color);
     if(color.equals("secondColor")){
         WebDriverWait wait = new WebDriverWait(driver, 10);
         wait.until(ExpectedConditions.elementToBeClickable(browser.vodafoneWebsite.product.getSecondColor()));
         browser.vodafoneWebsite.product.clickOnsecondColor();
         System.out.println("if"+availability);
     }
     else
     {
         System.out.println("else"+ availability);
     }
    }

    @Then("validate the product availability and add it to the cart")
    public void validateProductAvailabilityAndAddToCart() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(browser.vodafoneWebsite.product.getOutOfStock()));
            Assert.assertFalse(browser.vodafoneWebsite.product.getBuyNow().isEnabled());
            Assert.assertFalse(browser.vodafoneWebsite.product.getAddToCart().isEnabled());
            availability=false;
        } catch (Exception e) {
            availability=true;
        }
    }

    @When("click on add to cart")
    public void clickOnAddToCart() {
        if(availability=true){
            browser.vodafoneWebsite.product.clickOnAddToCart();
        }
    }

    @Then("the product added to cart successfully")
    public void productAddedToCartSuccessfully() {
        if(availability=true){
            Assert.assertTrue(browser.vodafoneWebsite.product.getAddedToCartAlert().isDisplayed());
        }
    }

    @When("click on cart button")
    public void clickOnCartButton() throws InterruptedException {
        Thread.sleep(5000);
        browser.vodafoneWebsite.cart=browser.vodafoneWebsite.home.clickOnCartButton();
    }

    @When("Click on Go to checkout button")
    public void clickOnGoToCheckoutButton() {
         browser.vodafoneWebsite.checkout=browser.vodafoneWebsite.cart.clickOnGoToCheckoutButton();
    }

    @When("Click on Add new Address")
    public void clickOnAddNewAddress() throws InterruptedException {
        Thread.sleep(5000);
         browser.vodafoneWebsite.checkout.clickOnAddNewAddressButton();
    }

    @When("Set city District and street name")
    public void setCityDistrictAndStreetName() {
        Select selectCity = new Select(browser.vodafoneWebsite.checkout.getCityDropDownList());
        Select selectDistirct= new Select(browser.vodafoneWebsite.checkout.getDistrictDropDownList());
        selectCity.selectByValue("3");
        selectDistirct.selectByValue("45");
        browser.vodafoneWebsite.checkout.enterStreetName("abbas el akkad");
    }

    @When("enter invalid Building {string} Floor {string} and Apartment values {string}")
    public void enterInvalidBuildingFloorAndApartmentValues(String building,String floor,String apartment) {
     browser.vodafoneWebsite.checkout.enterBuildingNumber(building);
     browser.vodafoneWebsite.checkout.enterFloorNumber(floor);
     browser.vodafoneWebsite.checkout.enterApartmentNumber(apartment);
    }

    @Then("Error message displayed and Save address button is dimmed")
    public void errorMessageDisplayedAndSaveAddressButtonDimmed() {

        Assert.assertTrue(browser.vodafoneWebsite.checkout.getErrorMessage().isDisplayed());
        Assert.assertFalse(browser.vodafoneWebsite.checkout.getSaveAddressButton().isEnabled());
    }
}
